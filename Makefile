CC=gcc
CFLAGS=-O2 -Wall -Werror -ansi -pedantic
LDFLAGS=

all: kst

kst: execute.o hashmap.o kst.o parser.o tower.o
	$(CC) $(LDFLAGS) -o kst execute.o hashmap.o kst.o parser.o tower.o

execute.o: execute.c execute.h ast.h hashmap.h tower.h
	$(CC) $(CFLAGS) -c -o execute.o execute.c

kst.o: kst.c ast.h parser.h execute.h hashmap.h
	$(CC) $(CFLAGS) -c -o kst.o kst.c

hashmap.o: hashmap.c hashmap.h ast.h
	$(CC) $(CFLAGS) -c -o hashmap.o hashmap.c

parser.o: parser.c ast.h hashmap.h
	$(CC) $(CFLAGS) -c -o parser.o parser.c

tower.o: tower.c ast.h tower.h
	$(CC) $(CFLAGS) -c -o tower.o tower.c

clean:
	find -name '*.o' -delete;
