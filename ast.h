#ifndef _AST_H_
#  define _AST_H_

#  include "tower.h"

struct ast;

enum AST_TYPE { A_VAR, A_UNARY, A_DUAL, A_FUNC, A_SEQ, A_WHILE,
  A_FOR
};

enum DATA_TYPE { KNIGHT_REF, COMPUTED_VAL };
typedef union val_union {
  DATA computed_val;
  int knight_ref;
} val_union;
typedef struct ast_var {
  enum DATA_TYPE type;
  val_union val;
} ast_var;

enum AST_UNARY_OP { NEGATION, CBOOL, CCHAR, NOT, NEXT, PREV };
typedef struct ast_unary {
  struct ast *arg;
  enum AST_UNARY_OP operator;
} ast_unary;

enum AST_DUAL_OP { SUM, DIFF, PRODUCT, DIV, MAX, MIN, EQ };
typedef struct ast_dual {
  struct ast *arg_1;
  struct ast *arg_2;
  enum AST_DUAL_OP operator;
} ast_dual;

enum AST_FUNC_OP { PUSH, ASSIGN, PRINT, INPUTC, INPUTN };
typedef struct ast_func {
  struct ast *arg_1;
  struct ast *arg_2;
  enum AST_FUNC_OP operator;
} ast_func;

struct ast_seq;
typedef struct ast_seq {
  struct ast *data;
  struct ast_seq *next;
} ast_seq;

typedef struct ast_while {
  struct ast *condition;
  struct ast_seq *commands;
} ast_while;

typedef struct ast_for {
  struct ast_var *variable;
  int num_knights;
  char **iterable_knights;
  int num_knights_remove;
  char **removable_knights;
  struct ast_seq *commands;
} ast_for;

typedef union amultival {
  ast_var *avar;
  ast_unary *aunary;
  ast_dual *adual;
  ast_func *afunc;
  ast_seq *aseq;
  ast_while *awhile;
  ast_for *afor;
} amultival;

typedef struct ast {
  enum AST_TYPE type;
  amultival adata;
} ast;

#endif
