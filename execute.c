#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "ast.h"
#include "execute.h"
#include "hashmap.h"
#include "tower.h"

DATA *eval_var(ast_var * avar)
{
  DATA *to_ret = malloc(sizeof (DATA));

  if (avar->type == KNIGHT_REF)
    memcpy(to_ret, knights + avar->val.knight_ref, sizeof (DATA));
  else                          /* if (avar->type == COMPUTED_VAL) */
    memcpy(to_ret, &(avar->val.computed_val), sizeof (DATA));

  return to_ret;
}

ast_var *eval_unary(ast_unary * aunary)
{
  DATA *e = evaluate(aunary->arg);

  ast_var *ret = malloc(sizeof (ast_var));
  DATA *dret = &(ret->val.computed_val);

  switch (aunary->operator) {
  case NEGATION:
    if (dret->type != KBOOL) {
      ret->type = COMPUTED_VAL;
      dret->type = e->type;
      dret->val = -1 * e->val;
      dret->val = -1 * e->val;
      break;
    }
    /*
     * fall
     */
  case NOT:
    ret->type = COMPUTED_VAL;
    dret->type = KBOOL;
    dret->val = (!(e->type == KBOOL && e->val == 1));
    break;
  case CBOOL:
    ret->type = COMPUTED_VAL;
    dret->type = KBOOL;
    dret->val = !(!(e->val));
    break;
  case CCHAR:
    ret->type = COMPUTED_VAL;
    dret->type = KCHAR;
    switch (e->type) {
    case KBOOL:
      dret->val = (e->val ? 'T' : 'F');
      break;
    case KINT:
      dret->val = (char) (e->val % 256);
      break;
    case KCHAR:
      dret->val = e->val;
      break;
    }
    break;
  case NEXT:
    ret->type = KNIGHT_REF;
    if (aunary->arg->type != A_VAR ||
        aunary->arg->adata.avar->type != KNIGHT_REF) {
      fprintf(stderr, "Fatal: next applied to something that was not a "
              "knight's name\n");
      exit(-1);
    }
    ret->val.knight_ref = aunary->arg->adata.avar->val.knight_ref + 1;
    if (ret->val.knight_ref == KNIGHT_NUM)
      ret->val.knight_ref = 1;
    break;
  case PREV:
    ret->type = KNIGHT_REF;
    if (aunary->arg->type != A_VAR ||
        aunary->arg->adata.avar->type != KNIGHT_REF) {
      fprintf(stderr, "Fatal: next applied to something that was not a "
              "knight's name\n");
      exit(-1);
    }
    ret->val.knight_ref = aunary->arg->adata.avar->val.knight_ref - 1;
    if (ret->val.knight_ref == 0)
      ret->val.knight_ref = KNIGHT_NUM - 1;
    break;
  }

  free(e);
  return ret;
}

ast_var *eval_dual(ast_dual * adual)
{
  ast_var *ret = malloc(sizeof (ast_var));
  DATA *dret = &(ret->val.computed_val);

  DATA *e1 = evaluate(adual->arg_1);
  DATA *e2 = evaluate(adual->arg_2);
  int v1, v2, v = 0;

  ret->type = COMPUTED_VAL;
  dret->type = (e1->type > e2->type ? e1->type : e2->type);

  if (e1->type == KBOOL)
    v1 = e1->val;
  else if (e1->type == KCHAR)
    v1 = e1->val;
  else                          /* if (e1->type == KINT) */
    v1 = e1->val;

  if (e2->type == KBOOL)
    v2 = e2->val;
  else if (e2->type == KCHAR)
    v2 = e2->val;
  else                          /* if (e2->type == KINT) */
    v2 = e2->val;

  switch (adual->operator) {
  case SUM:
    v = v1 + v2;
    break;
  case DIFF:
    v = v1 - v2;
    break;
  case PRODUCT:
    v = v1 * v2;
    break;
  case DIV:
    if (v2 == 0) {
      fprintf(stderr, "Fatal: division by 0\n");
      exit(-1);
    }
    v = v1 / v2;
    break;
  case MAX:
    v = (v1 > v2 ? v1 : v2);
    break;
  case MIN:
    dret->type = (e1->type < e2->type ? e1->type : e2->type);
    v = (v1 < v2 ? v1 : v2);
    break;
  case EQ:
    if (e1->type == e2->type && e1->val == e2->val)
      memcpy(ret, const_true->adata.avar, sizeof (ast_var));
    else
      memcpy(ret, const_false->adata.avar, sizeof (ast_var));
    return ret;
    break;
  }

  if (dret->type == KBOOL)
    dret->val = !(!(v));
  else if (dret->type == KCHAR)
    dret->val = (char) (v);
  else                          /* if (dret->type == KINT) */
    dret->val = v;

  free(e1);
  free(e2);
  return ret;
}

void eval_func(ast_func * afunc)
{
  int kidx, do_shuffle = 0, c, n;
  DATA *e;

  switch (afunc->operator) {
  case PUSH:
    tower_push(evaluate(afunc->arg_1));
    break;
  case ASSIGN:
    if (afunc->arg_1->type != A_VAR) {
      fprintf(stderr, "Fatal: cannot assign to something that is not a"
              " knight\n");
      exit(-1);
    }
    kidx = afunc->arg_1->adata.avar->val.knight_ref;
    if (kidx <= 0 || kidx > KNIGHT_NUM) {
      fprintf(stderr, "Fatal: no such knight %d\n", kidx);
      exit(-1);
    }

    e = evaluate(afunc->arg_2);
    memcpy(knights + kidx, e, sizeof (DATA));
    while (e->val == 0) {
      do_shuffle = 1;
      free(e);
      e = tower_pop();
      memcpy(knights + kidx, e, sizeof (DATA));
    }
    if (do_shuffle)
      shuffle();
    break;
  case PRINT:
    if (afunc->arg_1->type != A_VAR) {
      fprintf(stderr, "Fatal: cannot print something that is not a"
              " knight\n");
      exit(-1);
    }
    kidx = afunc->arg_1->adata.avar->val.knight_ref;
    if (kidx <= 0 || kidx > KNIGHT_NUM) {
      fprintf(stderr, "Fatal: no such knight %d\n", kidx);
      exit(-1);
    }

    e = knights + kidx;
    if (e->type == KBOOL)
      if (e->val)
        printf("true");
      else
        printf("false");
    else if (e->type == KCHAR)
      printf("%c", e->val);
    else                        /* if (e->type == KINT) */
      printf("%d", e->val);

    do {
      e = tower_pop();
      memcpy(knights + kidx, e, sizeof (DATA));
    } while ((e->type == KBOOL && e->val == 0) ||
             (e->type == KCHAR && e->val == 0) ||
             (e->type == KINT && e->val == 0));

    shuffle();
    break;
  case INPUTC:
    e = malloc(sizeof (DATA));
    c = fgetc(stdin);
    if (c == EOF) {
      e->type = KBOOL;
      e->val = 0;
    } else {
      e->type = KCHAR;
      e->val = c;
    }
    tower_push(e);
    break;
  case INPUTN:
    e = malloc(sizeof (DATA));
    e->type = KINT;
    n = 0;
    while (1) {
      c = fgetc(stdin);
      if (c == EOF || c == '\n')
        break;
      if (c < '0' || c > '9') {
        fprintf(stderr, "Fatal: cannot convert string containing '%c' to"
                " int\n", c);
      }
      n *= 10;
      n += (c - '0');
    }
    e->val = n;
    tower_push(e);
    break;
  }
}

void eval_seq(ast_seq * aseq)
{
  while (aseq) {
    evaluate(aseq->data);
    aseq = aseq->next;
  }
}

void eval_while(ast_while * awhile)
{
  DATA *e;

  while (1) {
    e = evaluate(awhile->condition);
    if (!e->val)
      break;
    free(e);

    eval_seq(awhile->commands);
  }
  free(e);
}

void eval_for(ast_for * afor)
{
  int n, m, eval;
  char **kiter, **kiter_r;

  afor->variable->type = KNIGHT_REF;

  for (kiter = afor->iterable_knights, n = 0; n < afor->num_knights;
       ++n, ++kiter) {
    eval = 1;
    if (kiter && *kiter) {
      memcpy(afor->variable, find_var(*kiter), sizeof (ast_var));
      for (kiter_r = afor->removable_knights, m = 0;
           m < afor->num_knights_remove; ++m, ++kiter_r) {
        if (find_var(*kiter_r)->val.knight_ref ==
            afor->variable->val.knight_ref)
          eval = 0;
        break;
      }

      if (eval)
        eval_seq(afor->commands);
    }
  }
}

DATA *evaluate(ast * a)
{
  ast_var *aret;
  DATA *dret;

  if (!a)
    return NULL;

  switch (a->type) {
  case A_VAR:
    return eval_var(a->adata.avar);
  case A_UNARY:
    aret = eval_unary(a->adata.aunary);
    dret = eval_var(aret);
    free(aret);
    return dret;
  case A_DUAL:
    aret = eval_dual(a->adata.adual);
    dret = eval_var(aret);
    free(aret);
    return dret;
  case A_FUNC:
    eval_func(a->adata.afunc);
    return NULL;
  case A_SEQ:
    eval_seq(a->adata.aseq);
    return NULL;
  case A_FOR:
    eval_for(a->adata.afor);
    return NULL;
  case A_WHILE:
    eval_while(a->adata.awhile);
    return NULL;
  }

  return NULL;
}
