#include <stdio.h>
#include <stdlib.h>

#include "hashmap.h"

struct hashlevel;

typedef struct hashlevel {
  ast_var *data;
  struct hashlevel *sublevels;
} hashlevel;

static hashlevel *root;

void hashmap_initialize()
{
  ast_var *temp;

  root = calloc(1, sizeof (hashlevel));
  temp = find_var("one");
  temp->type = KNIGHT_REF;
  temp->val.knight_ref = 1;

  temp = find_var("two");
  temp->type = KNIGHT_REF;
  temp->val.knight_ref = 2;

  temp = find_var("three");
  temp->type = KNIGHT_REF;
  temp->val.knight_ref = 3;

  temp = find_var("four");
  temp->type = KNIGHT_REF;
  temp->val.knight_ref = 4;

  temp = find_var("five");
  temp->type = KNIGHT_REF;
  temp->val.knight_ref = 5;

  temp = find_var("six");
  temp->type = KNIGHT_REF;
  temp->val.knight_ref = 6;

  temp = find_var("seven");
  temp->type = KNIGHT_REF;
  temp->val.knight_ref = 7;

  temp = find_var("eight");
  temp->type = KNIGHT_REF;
  temp->val.knight_ref = 8;

  temp = find_var("nine");
  temp->type = KNIGHT_REF;
  temp->val.knight_ref = 9;

}

static int index(const char c)
{
  int i = c;

  if (c >= '0')
    i -= '0';
  if (c > 'a')
    i -= ('a' - '0');
  return i;
}

ast_var *find_var(const char *key)
{
  const char *c_key = key;
  hashlevel *c_hash = root;
  int c_idx;

  if (!key)
    return NULL;

  while (*c_key != '\0') {
    c_idx = index(*c_key);
    if (!c_hash->sublevels)
      c_hash->sublevels = calloc(37, sizeof (hashlevel));
    c_hash = &(c_hash->sublevels[c_idx]);
    c_key++;
  }

  if (!c_hash->data)
    c_hash->data = calloc(1, sizeof (ast_var));

  return c_hash->data;
}
