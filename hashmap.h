#ifndef _HASHMAP_H_
#  define _HASHMAP_H_

#  include "ast.h"

ast_var *find_var(const char *key);

void hashmap_initialize();

#endif
