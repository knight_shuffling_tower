#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "ast.h"
#include "parser.h"
#include "execute.h"
#include "hashmap.h"

int main(int argc, char **argv)
{
  if (argc != 2) {
    fprintf(stderr, "Usage: %s INPUT_FILE\n", argv[0]);
    return -1;
  }

  srand(time(NULL));
  hashmap_initialize();
  tower_initialize();

  evaluate(build(fopen(argv[1], "r")));

  return 0;
}
