#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "ast.h"
#include "hashmap.h"

#define T_DIV 0
#define T_EQ 1
#define T_GETS 2
#define T_MINUS 3
#define T_PLUS 4
#define T_PROD 5
#define T_LPAREN 6
#define T_RPAREN 7
#define T_AS 8
#define T_ALL 9
#define T_BOOL 10
#define T_BUT 11
#define T_CHAR 12
#define T_DO 13
#define T_DONE 14
#define T_FALSE 15
#define T_FOR 16
#define T_INPUTC 17
#define T_INPUTN 18
#define T_MAX 19
#define T_MIN 20
#define T_NEXT 21
#define T_NOT 22
#define T_PREV 23
#define T_PRINT 24
#define T_PUSH 25
#define T_TRUE 26
#define T_WHILE 27
#define T_COMBEGIN 28
#define T_COMEND 29
#define T_EOF 30
#define T_UNKNOWN 31
#define T_VARNAME 32

typedef short TOKEN;

static FILE *in;
static char *last_string_seen;
static int lss_sz = 0, token_pushed = 0, lineno = 1;
static TOKEN forced_token;

void was_expecting(const char *s)
{
  fprintf(stderr, "Line %d: Fatal: was expecting %s\n", lineno, s);
  exit(-1);
}

void unexpected(const char *s)
{
  fprintf(stderr, "Line %d: Fatal: unexpected %s\n", lineno, s);
  exit(-1);
}

int next_char()
{
  int c = 0;

  while (c == 0)
    c = fgetc(in);

  if (c == '\n')
    lineno++;

  if (c >= 'A' && c <= 'Z')
    return c - 'A' + 'a';
  if (c == '\t' || c == '\n')
    return ' ';

  return c;
}

static TOKEN nt_varname(const char *prefix, char first)
{
  int pre_len = strlen(prefix);
  int min_len = ((31 > pre_len) ? 31 : (2 * pre_len)) + 1;
  int slen = pre_len;
  char *ip, c = first;

  if (!((c <= 'z' && c >= 'a') || (c <= '9' && c >= '0') || c == '_')) {
    return T_UNKNOWN;
  }

  if (last_string_seen == NULL) {
    last_string_seen = malloc(sizeof (char) * min_len);
    lss_sz = min_len;
  } else if (min_len > lss_sz) {
    last_string_seen = realloc(last_string_seen, sizeof (char) * min_len);
    lss_sz = min_len;
  }

  strcpy(last_string_seen, prefix);
  ip = last_string_seen + pre_len;

  while (1) {
    if (!((c <= 'z' && c >= 'a') || (c <= '9' && c >= '0') || c == '_'))
      break;

    *ip++ = c;
    if (++slen >= lss_sz) {
      lss_sz *= 2;
      last_string_seen = realloc(last_string_seen, sizeof (char) * lss_sz);
      ip = last_string_seen + slen;
    }

    c = next_char();
  }

  ungetc(c, in);
  *ip = '\0';

  return T_VARNAME;
}

static TOKEN nt_a()
{
  int c;

  switch (c = next_char()) {
  case 's':
    switch (c = next_char()) {
    case EOF:
    case ' ':
      return T_AS;
    default:
      return nt_varname("as", c);
    }
  case 'l':
    switch (c = next_char()) {
    case 'l':
      switch (c = next_char()) {
      case EOF:
      case ' ':
        return T_ALL;
      default:
        return nt_varname("all", c);
      }
    default:
      return nt_varname("al", c);
    }
  default:
    return nt_varname("a", c);
  }
}

static TOKEN nt_b()
{
  int c;

  switch (c = next_char()) {
  case 'o':
    switch (c = next_char()) {
    case 'o':
      switch (c = next_char()) {
      case 'l':
        switch (c = next_char()) {
        case EOF:
        case ' ':
          return T_BOOL;
        default:
          return nt_varname("bool", c);
        }
      default:
        return nt_varname("boo", c);
      }
    default:
      return nt_varname("bo", c);
    }
  case 'u':
    switch (c = next_char()) {
    case 't':
      switch (c = next_char()) {
      case EOF:
      case ' ':
        return T_BUT;
      default:
        return nt_varname("but", c);
      }
    default:
      return nt_varname("bu", c);
    }
  default:
    return nt_varname("b", c);
  }
}

static TOKEN nt_c()
{
  int c;

  switch (c = next_char()) {
  case 'h':
    switch (c = next_char()) {
    case 'a':
      switch (c = next_char()) {
      case 'r':
        switch (c = next_char()) {
        case EOF:
        case ' ':
          return T_CHAR;
        default:
          return nt_varname("char", c);
        }
      default:
        return nt_varname("cha", c);
      }
    default:
      return nt_varname("ch", c);
    }
  default:
    return nt_varname("c", c);
  }
}

static TOKEN nt_d()
{
  int c;

  switch (c = next_char()) {
  case 'o':
    switch (c = next_char()) {
    case EOF:
    case ' ':
      return T_DO;
    case 'n':
      switch (c = next_char()) {
      case 'e':
        switch (c = next_char()) {
        case EOF:
        case ' ':
          return T_DONE;
        default:
          return nt_varname("done", c);
        }
      default:
        return nt_varname("don", c);
      }
    default:
      return nt_varname("do", c);
    }
  default:
    return nt_varname("d", c);
  }
}

static TOKEN nt_f()
{
  int c;

  switch (c = next_char()) {
  case 'o':
    switch (c = next_char()) {
    case 'r':
      switch (c = next_char()) {
      case EOF:
      case ' ':
        return T_FOR;
      default:
        return nt_varname("for", c);
      }
    default:
      return nt_varname("fo", c);
    }
  case 'a':
    switch (c = next_char()) {
    case 'l':
      switch (c = next_char()) {
      case 's':
        switch (c = next_char()) {
        case 'e':
          switch (c = next_char()) {
          case EOF:
          case ' ':
            return T_FALSE;
          default:
            return nt_varname("false", c);
          }
        default:
          return nt_varname("fals", c);
        }
      default:
        return nt_varname("fal", c);
      }
    default:
      return nt_varname("fa", c);
    }
  default:
    return nt_varname("f", c);
  }
}

static TOKEN nt_i()
{
  int c;

  switch (c = next_char()) {
  case 'n':
    switch (c = next_char()) {
    case 'p':
      switch (c = next_char()) {
      case 'u':
        switch (c = next_char()) {
        case 't':
          switch (c = next_char()) {
          case 'c':
            switch (c = next_char()) {
            case EOF:
            case ' ':
              return T_INPUTC;
            default:
              return nt_varname("inputc", c);
            }
          case 'n':
            switch (c = next_char()) {
            case EOF:
            case ' ':
              return T_INPUTN;
            default:
              return nt_varname("inputn", c);
            }
          default:
            return nt_varname("input", c);
          }
        default:
          return nt_varname("inpu", c);
        }
      default:
        return nt_varname("inp", c);
      }
    default:
      return nt_varname("in", c);
    }
  default:
    return nt_varname("i", c);
  }
}

static TOKEN nt_m()
{
  int c;

  switch (c = next_char()) {
  case 'a':
    switch (c = next_char()) {
    case 'x':
      switch (c = next_char()) {
      case EOF:
      case ' ':
        return T_MAX;
      default:
        return nt_varname("max", c);
      }
    default:
      return nt_varname("ma", c);
    }
  case 'i':
    switch (c = next_char()) {
    case 'n':
      switch (c = next_char()) {
      case EOF:
      case ' ':
        return T_MIN;
      default:
        return nt_varname("min", c);
      }
    default:
      return nt_varname("mi", c);
    }
  default:
    return nt_varname("m", c);
  }
}

static TOKEN nt_n()
{
  int c;

  switch (c = next_char()) {
  case 'e':
    switch (c = next_char()) {
    case 'x':
      switch (c = next_char()) {
      case 't':
        switch (c = next_char()) {
        case EOF:
        case ' ':
          return T_NEXT;
        default:
          return nt_varname("next", c);
        }
      default:
        return nt_varname("nex", c);
      }
    default:
      return nt_varname("ne", c);
    }
  case 'o':
    switch (c = next_char()) {
    case 't':
      switch (c = next_char()) {
      case EOF:
      case ' ':
        return T_NOT;
      default:
        return nt_varname("not", c);
      }
    default:
      return nt_varname("no", c);
    }
  default:
    return nt_varname("n", c);
  }
}

static TOKEN nt_p()
{
  int c;

  switch (c = next_char()) {
  case 'r':
    switch (c = next_char()) {
    case 'e':
      switch (c = next_char()) {
      case 'v':
        switch (c = next_char()) {
        case EOF:
        case ' ':
          return T_PREV;
        default:
          return nt_varname("prev", c);
        }
      default:
        return nt_varname("pre", c);
      }
    case 'i':
      switch (c = next_char()) {
      case 'n':
        switch (c = next_char()) {
        case 't':
          switch (c = next_char()) {
          case EOF:
          case ' ':
            return T_PRINT;
          default:
            return nt_varname("print", c);
          }
        default:
          return nt_varname("prin", c);
        }
      default:
        return nt_varname("pri", c);
      }
    default:
      return nt_varname("pr", c);
    }
  case 'u':
    switch (c = next_char()) {
    case 's':
      switch (c = next_char()) {
      case 'h':
        switch (c = next_char()) {
        case EOF:
        case ' ':
          return T_PUSH;
        default:
          return nt_varname("push", c);
        }
      default:
        return nt_varname("pus", c);
      }
    default:
      return nt_varname("pu", c);
    }
  default:
    return nt_varname("p", c);
  }
}

static TOKEN nt_t()
{
  int c;

  switch (c = next_char()) {
  case 'r':
    switch (c = next_char()) {
    case 'u':
      switch (c = next_char()) {
      case 'e':
        switch (c = next_char()) {
        case EOF:
        case ' ':
          return T_TRUE;
        default:
          return nt_varname("true", c);
        }
      default:
        return nt_varname("tru", c);
      }
    default:
      return nt_varname("tr", c);
    }
  default:
    return nt_varname("t", c);
  }
}

static TOKEN nt_w()
{
  int c;

  switch (c = next_char()) {
  case 'h':
    switch (c = next_char()) {
    case 'i':
      switch (c = next_char()) {
      case 'l':
        switch (c = next_char()) {
        case 'e':
          switch (c = next_char()) {
          case EOF:
          case ' ':
            return T_WHILE;
          default:
            return nt_varname("while", c);
          }
        default:
          return nt_varname("whil", c);
        }
      default:
        return nt_varname("whi", c);
      }
    default:
      return nt_varname("wh", c);
    }
  default:
    return nt_varname("w", c);
  }
}

static TOKEN nt_lparen()
{
  char c = next_char();

  if (c == '*')
    return T_COMBEGIN;

  ungetc(c, in);
  return T_LPAREN;
}

static TOKEN nt_asterisk()
{
  if (next_char() == ')')
    return T_COMEND;
  else
    return T_PROD;
}

static TOKEN nt_lt()
{
  if (next_char() == '-')
    return T_GETS;
  else
    return T_UNKNOWN;
}

TOKEN next_raw_tok()
{
  int c = next_char();

  while (c == ' ')
    c = next_char();

  switch (c) {
  case EOF:
    return T_EOF;
  case '+':
    return T_PLUS;
  case '-':
    return T_MINUS;
  case '/':
    return T_DIV;
  case '=':
    return T_EQ;
  case ')':
    return T_RPAREN;
  case '(':
    return nt_lparen();
  case '*':
    return nt_asterisk();
  case '<':
    return nt_lt();
  case 'a':
    return nt_a();
  case 'b':
    return nt_b();
  case 'c':
    return nt_c();
  case 'd':
    return nt_d();
  case 'f':
    return nt_f();
  case 'i':
    return nt_i();
  case 'm':
    return nt_m();
  case 'n':
    return nt_n();
  case 'p':
    return nt_p();
  case 't':
    return nt_t();
  case 'w':
    return nt_w();
  default:
    return nt_varname("", c);
  }
}

void push_back_token(TOKEN t)
{
  token_pushed = 1;
  forced_token = t;
}

TOKEN next_token()
{
  TOKEN t;
  int comment_depth = 0;

  if (token_pushed) {
    token_pushed = 0;
    return forced_token;
  }

  t = next_raw_tok();

  if (t == T_COMBEGIN) {
    comment_depth++;
    t = next_raw_tok();
    do {
      if (t == T_COMBEGIN)
        comment_depth++;
      else if (t == T_COMEND) {
        if (comment_depth == 0) {
          unexpected("*)");
        } else
          comment_depth--;
      } else if (t == T_EOF) {
        fprintf(stderr, "Line %d: Fatal: unterminated comment\n", lineno);
        exit(-1);
      }
      t = next_raw_tok();
    } while (comment_depth > 0 || t == T_COMBEGIN);
  }

  return t;
}

/* binding levels.  5 is tightest binding (parens, vars), 0 is loosest
   (eq testing) */
ast *expr_0();
ast *expr_1();
ast *expr_2();
ast *expr_3();
ast *expr_4();
ast *expr_5();

ast *expr_0()
{
  ast *e1 = expr_1();
  ast *to_return;
  TOKEN t = next_token();

  if (t == T_EQ) {
    to_return = malloc(sizeof (ast));
    to_return->type = A_DUAL;
    to_return->adata.adual = malloc(sizeof (ast_dual));
    to_return->adata.adual->operator = EQ;
    to_return->adata.adual->arg_1 = e1;
    to_return->adata.adual->arg_2 = expr_1();
  } else {
    push_back_token(t);
    to_return = e1;
  }

  return to_return;
}

ast *expr_1()
{
  TOKEN t = next_token();
  ast *to_return;

  if (t == T_MINUS || t == T_TRUE || t == T_FALSE || t == T_VARNAME
      || t == T_LPAREN) {
    push_back_token(t);
    return expr_2();
  }

  to_return = malloc(sizeof (ast));
  if (t == T_MIN || t == T_MAX) {
    to_return->type = A_DUAL;
    to_return->adata.adual = malloc(sizeof (ast_dual));
    to_return->adata.adual->operator =(t == T_MIN) ? MIN : MAX;
    to_return->adata.adual->arg_1 = expr_2();
    to_return->adata.adual->arg_2 = expr_2();
  } else if (t == T_NOT || t == T_BOOL || t == T_CHAR || t == T_NEXT
             || t == T_PREV) {
    to_return->type = A_UNARY;
    to_return->adata.aunary = malloc(sizeof (ast_unary));
    switch (t) {
    case T_NOT:
      to_return->adata.aunary->operator = NOT;
      break;
    case T_BOOL:
      to_return->adata.aunary->operator = CBOOL;
      break;
    case T_CHAR:
      to_return->adata.aunary->operator = CCHAR;
      break;
    case T_NEXT:
      to_return->adata.aunary->operator = NEXT;
      break;
    case T_PREV:
      to_return->adata.aunary->operator = PREV;
      break;
    }
    to_return->adata.aunary->arg = expr_2();
  } else {
    was_expecting("operator, -, constant, variable name, or (");
  }
  return to_return;
}

ast *expr_2()
{
  ast *e1 = expr_3();
  ast *to_return = e1;
  TOKEN t = next_token();

  while (t == T_PLUS || t == T_MINUS) {
    e1 = to_return;
    to_return = malloc(sizeof (ast));
    to_return->type = A_DUAL;
    to_return->adata.adual = malloc(sizeof (ast_dual));
    to_return->adata.adual->operator =(t == T_PLUS) ? SUM : DIFF;
    to_return->adata.adual->arg_1 = e1;
    to_return->adata.adual->arg_2 = expr_3();
    t = next_token();
  }
  push_back_token(t);
  return to_return;
}

ast *expr_3()
{
  ast *e1 = expr_4();
  ast *to_return = e1;
  TOKEN t = next_token();

  while (t == T_PROD || t == T_DIV) {
    e1 = to_return;
    to_return = malloc(sizeof (ast));
    to_return->type = A_DUAL;
    to_return->adata.adual = malloc(sizeof (ast_dual));
    to_return->adata.adual->operator =(t == T_PROD) ? PRODUCT : DIV;
    to_return->adata.adual->arg_1 = e1;
    to_return->adata.adual->arg_2 = expr_4();
    t = next_token();
  }
  push_back_token(t);
  return to_return;
}

ast *expr_4()
{
  TOKEN t = next_token();
  ast *to_return;

  if (t == T_MINUS) {
    to_return = malloc(sizeof (ast));
    to_return->type = A_UNARY;
    to_return->adata.aunary = malloc(sizeof (ast_unary));
    to_return->adata.aunary->operator = NEGATION;
    to_return->adata.aunary->arg = expr_5();
    return to_return;
  }

  push_back_token(t);
  return expr_5();
}

ast *expr_5()
{
  TOKEN t = next_token();
  ast *to_return;

  if (t == T_TRUE)
    return const_true;
  if (t == T_FALSE)
    return const_false;
  if (t == T_VARNAME) {
    to_return = malloc(sizeof (ast));
    to_return->type = A_VAR;
    to_return->adata.avar = find_var(last_string_seen);
    return to_return;
  }
  if (t == T_LPAREN) {
    to_return = expr_0();
    if (next_token() != T_RPAREN)
      was_expecting(")");
    return to_return;
  }
  was_expecting("expression");
  return NULL;
}

ast *build_expr()
{
  return expr_0();
}

ast_func *build_func(TOKEN first)
{
  ast_func *to_return = malloc(sizeof (ast_func));

  switch (first) {
  case T_PUSH:
    to_return->operator = PUSH;
    to_return->arg_1 = build_expr(0);
    break;
  case T_PRINT:
    to_return->operator = PRINT;
    to_return->arg_1 = build_expr(0);
    break;
  case T_INPUTC:
    to_return->operator = INPUTC;
    break;
  case T_INPUTN:
    to_return->operator = INPUTN;
    break;
  case T_VARNAME:
    to_return->operator = ASSIGN;
    to_return->arg_1 = malloc(sizeof (ast));
    if (next_token() != T_GETS)
      was_expecting("assignment");
    to_return->arg_1->type = A_VAR;
    to_return->arg_1->adata.avar = find_var(last_string_seen);
    to_return->arg_2 = build_expr(0);
    break;
  default:
    was_expecting("known function");
  }

  return to_return;
}

ast_seq *build_seq();

ast_while *build_while()
{
  ast_while *to_return = malloc(sizeof (ast_while));

  to_return->condition = build_expr(0);

  if (next_token() != T_DO)
    was_expecting("do");

  to_return->commands = build_seq(1);

  return to_return;
}

int stoi(const char *s)
{
  if (!strcmp(s, "one"))
    return 1;
  if (!strcmp(s, "two"))
    return 2;
  if (!strcmp(s, "three"))
    return 3;
  if (!strcmp(s, "four"))
    return 4;
  if (!strcmp(s, "five"))
    return 5;
  if (!strcmp(s, "six"))
    return 6;
  if (!strcmp(s, "seven"))
    return 7;
  if (!strcmp(s, "eight"))
    return 8;
  if (!strcmp(s, "nine"))
    return 9;
  return -1;
}

const char *itos(int i)
{
  switch (i) {
  case 1:
    return "one";
  case 2:
    return "two";
  case 3:
    return "three";
  case 4:
    return "four";
  case 5:
    return "five";
  case 6:
    return "six";
  case 7:
    return "seven";
  case 8:
    return "eight";
  case 9:
    return "nine";
  default:
    return "none";
  }
}

ast_for *build_for()
{
  ast_for *to_return = malloc(sizeof (ast_for));
  int k1 = -1, k2 = -1, ktmp, is_range, is_remove = 0;
  const char *kref = NULL;
  int max_knights = 8, max_knights_remove = 8;
  char c;
  TOKEN t;

  to_return->num_knights = 0;
  to_return->num_knights_remove = 0;
  to_return->iterable_knights = malloc(max_knights * sizeof (char *));
  to_return->removable_knights = malloc(max_knights_remove * sizeof (char *));

  do {
    is_range = 0;
    t = next_token();
    switch (t) {
    case T_ALL:
      k1 = 1;
      k2 = 9;
      is_range = 1;
      break;
    case T_BUT:
      is_remove = 1;
      break;
    case T_VARNAME:
      k1 = stoi(last_string_seen);
      if ((c = next_char()) == '.') {
        if (k1 < 0)
          was_expecting("literal knight name");
        if ((c = next_char()) == '.') {
          is_range = 1;
          t = next_token();
          if (t != T_VARNAME)
            was_expecting("range-ending knight reference");
          k2 = stoi(last_string_seen);
          if (k2 < 0)
            was_expecting("literal knight reference ('one', 'eight', etc)");
        } else {
          was_expecting("..");
        }
      } else {
        kref = last_string_seen;
      }
      break;
    case T_AS:
      break;
    default:
      was_expecting("variable name, all, but, or as");
    }

    if (t != T_AS && t != T_BUT) {
      if (is_range) {
        if (k1 >= k2) {
          fprintf(stderr, "Line %d: Fatal: range %d..%d is invalid\n", lineno,
                  k1, k2);
          exit(-1);
        }

        if (!is_remove) {
          if (to_return->num_knights + (k2 - k1 + 1) >= max_knights) {
            max_knights += k2 - k1;
            to_return->iterable_knights = realloc(to_return->iterable_knights,
                                                  max_knights *
                                                  sizeof (char *));
          }
        } else {
          if (to_return->num_knights_remove + (k2 - k1 + 1) >=
              max_knights_remove) {
            max_knights_remove += k2 - k1;
            to_return->removable_knights = realloc(to_return->removable_knights,
                                                   max_knights_remove *
                                                   sizeof (char *));
          }
        }

        if (!is_remove) {
          for (ktmp = k1; ktmp <= k2; ++ktmp) {
            kref = itos(ktmp);
            to_return->iterable_knights[to_return->num_knights] =
                malloc(sizeof (char) * (strlen(kref) + 1));
            strcpy(to_return->iterable_knights[to_return->num_knights], kref);
            to_return->num_knights++;
          }
          max_knights += (k2 - k1 + 1);
        } else {
          for (ktmp = k1; ktmp <= k2; ++ktmp) {
            kref = itos(ktmp);
            to_return->removable_knights[to_return->num_knights_remove] =
                malloc(sizeof (char) * (strlen(kref) + 1));
            strcpy(to_return->removable_knights[to_return->num_knights_remove],
                   kref);
            to_return->num_knights_remove++;
          }
          max_knights_remove += (k2 - k1 + 1);
        }

      } else {
        if (!is_remove) {
          if (to_return->num_knights + 1 >= max_knights) {
            max_knights *= 2;
            to_return->iterable_knights = realloc(to_return->iterable_knights,
                                                  max_knights *
                                                  sizeof (char *));
          }
          to_return->iterable_knights[to_return->num_knights] =
              malloc(sizeof (char) * (strlen(kref) + 1));
          strcpy(to_return->iterable_knights[to_return->num_knights], kref);
          to_return->num_knights++;
        } else {
          if (to_return->num_knights_remove + 1 >= max_knights_remove) {
            max_knights_remove *= 2;
            to_return->removable_knights = realloc(to_return->removable_knights,
                                                   max_knights_remove *
                                                   sizeof (char *));
          }
          to_return->removable_knights[to_return->num_knights_remove] =
              malloc(sizeof (char) * (strlen(kref) + 1));
          strcpy(to_return->removable_knights[to_return->num_knights_remove],
                 kref);
          to_return->num_knights_remove++;
        }
      }
    }
  } while (t != T_AS);

  t = next_token();
  if (t != T_VARNAME)
    was_expecting("variable");

  to_return->variable = find_var(last_string_seen);

  t = next_token();
  if (t != T_DO)
    was_expecting("do");

  to_return->commands = build_seq(1);

  return to_return;
}

ast_seq *build_seq(int expect_done)
{
  ast_seq *to_return = calloc(1, sizeof (ast_seq));
  ast_seq *current = to_return;
  TOKEN t;

  while (1) {
    t = next_token();
    if (t == T_DONE || t == T_EOF) {
      if (expect_done == (t == T_DONE)) {
        return to_return;
      } else {
        unexpected("done or EOF");
      }
    }
    current->data = calloc(1, sizeof (ast));
    if (t == T_WHILE) {
      current->data->type = A_WHILE;
      current->data->adata.awhile = build_while();
    } else if (t == T_FOR) {
      current->data->type = A_FOR;
      current->data->adata.afor = build_for();
    } else {
      current->data->type = A_FUNC;
      current->data->adata.afunc = build_func(t);
    }
    current->next = calloc(1, sizeof (ast_seq));
    current = current->next;
  }
}

ast *build(FILE * input)
{
  ast *to_return = malloc(sizeof (ast));

  if (!input) {
    fprintf(stderr, "Could not open input file\n");
    exit(-1);
  }

  in = input;

  to_return->type = A_SEQ;
  to_return->adata.aseq = build_seq(0);
  return to_return;
}
