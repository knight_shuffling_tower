#include <stdlib.h>
#include <stdio.h>

#include "ast.h"
#include "tower.h"

struct fifo_node;
typedef struct fifo_node {
  DATA *data;
  struct fifo_node *prev;
} fifo_node;

static fifo_node *tower_entrance = NULL;
static fifo_node *tower_exit = NULL;

struct ast *const_true;
struct ast *const_false;
DATA knights[KNIGHT_NUM + 1];

void tower_push(DATA * d)
{
  fifo_node *new_node;

  if (!tower_entrance) {
    tower_entrance = malloc(sizeof (fifo_node));
    if (!tower_entrance) {
      fprintf(stderr, "Fatal: Could not allocate memory for tower\n");
      exit(-1);
    }

    tower_exit = tower_entrance;
    tower_entrance->data = d;
    tower_entrance->prev = NULL;
  } else {
    new_node = malloc(sizeof (fifo_node));
    if (!new_node) {
      fprintf(stderr, "Fatal: Could not allocate memory for tower\n");
      exit(-1);
    }

    new_node->data = d;
    new_node->prev = NULL;
    tower_entrance->prev = new_node;
    tower_entrance = new_node;
  }
}

DATA *tower_pop()
{
  DATA *to_return = NULL;
  fifo_node *to_delete;

  if (!tower_exit)
    exit(0);

  to_return = tower_exit->data;

  if (tower_entrance == tower_exit) {
    free(tower_exit);
    tower_entrance = NULL;
    tower_exit = NULL;
  } else {
    to_delete = tower_exit;
    tower_exit = tower_exit->prev;
    free(to_delete);
  }

  return to_return;
}

void shuffle()
{
  int idx, tidx;
  DATA t;

  for (idx = 1; idx <= KNIGHT_NUM; ++idx) {
    tidx = 1 + rand() % KNIGHT_NUM;
    t = knights[tidx];
    knights[tidx] = knights[idx];
    knights[idx] = t;
  }
}

void tower_initialize()
{
  int i;

  const_true = malloc(sizeof (ast));
  const_true->type = A_VAR;
  const_true->adata.avar = malloc(sizeof (ast_var));
  const_true->adata.avar->type = COMPUTED_VAL;
  const_true->adata.avar->val.computed_val.type = KBOOL;
  const_true->adata.avar->val.computed_val.val = 1;

  const_false = malloc(sizeof (ast));
  const_false->type = A_VAR;
  const_false->adata.avar = malloc(sizeof (ast_var));
  const_false->adata.avar->type = COMPUTED_VAL;
  const_false->adata.avar->val.computed_val.type = KBOOL;
  const_false->adata.avar->val.computed_val.val = 0;

  for (i = 1; i <= 9; ++i) {
    knights[i].type = KINT;
    knights[i].val = i;
  }
  shuffle();
}
