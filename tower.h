#ifndef _TOWER_H_
#  define _TOWER_H_

enum VAR_TYPE { KCHAR, KBOOL, KINT };

typedef struct DATA {
  enum VAR_TYPE type;
  int val;
} DATA;

#  define KNIGHT_NUM 9

void tower_initialize();
void tower_push(DATA *);
DATA *tower_pop();
void shuffle();

extern DATA knights[];

struct ast;
extern struct ast *const_true;
extern struct ast *const_false;

#endif
